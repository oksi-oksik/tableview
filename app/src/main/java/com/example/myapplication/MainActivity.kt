package com.example.myapplication

import android.graphics.drawable.Drawable
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var btn: Button
    private lateinit var image: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn = findViewById(R.id.nextButton)
        image = findViewById(R.id.imageView)
        /*btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                image.setImageResource(R.drawable.image2)
            }
        })*/
        var last_num = 10
        btn.setOnClickListener {
            var num = (1..7).random()
            while (num == last_num){
                num = (1..7).random()
            }
            when (num){
                1->image.setImageResource(R.drawable.image1)
                2->image.setImageResource(R.drawable.image2)
                3->image.setImageResource(R.drawable.image3)
                4->image.setImageResource(R.drawable.image4)
                5->image.setImageResource(R.drawable.image5)
                6->image.setImageResource(R.drawable.image6)
                7->image.setImageResource(R.drawable.image7)
            }
            last_num = num
        }
    }

}